<?php
namespace App\ User;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
session_start();
//include_once('../../vendor/autoload.php');

class User extends DB{
    public $table="users";
    public $firstname="";
    public $lastname="";
    public $email="";
    public $password="";
    public $id;

    public function __construct()
    {
        parent::__construct();
    }

    public function prepare($data){
        if(array_key_exists('email',$data)){
            $this->email=$data['email'];
        }
        if(array_key_exists('password',$data)){
            $this->password=md5($data['password']);
        }
        if(array_key_exists('first_name',$data)){
            $this->firstname=$data['first_name'];
        }
        if(array_key_exists('last_name',$data)){
            $this->lastname=$data['last_name'];
        }
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        return $this;
    }

    public function store(){
        $query="INSERT INTO `users` (`first_name`, `last_name`, `email`, `password`) VALUES ('".$this->firstname."', '".$this->lastname."', '".$this->email."', '".$this->password."')";
        $result=mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class='alert alert-success'>Stored!</div>");
            Utility::redirect("../../index.php");

        }
        else{
            Utility::redirect("../../index.php");

        }
    }

    public function view(){
        $query="SELECT * FROM `users` WHERE `email`='".$this->email;
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }



}