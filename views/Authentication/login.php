<?php
include_once("../../vendor/autoload.php");
session_start();
use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;
$auth=new Auth();
$status= $auth->prepare($_POST)->is_registered();
if($status){
    $_SESSION['user_email']=$_POST['email'];
    Utility::redirect("../welcome.php");

}
else{
    Message::message("<div class='alert alert-success'><strong>Failed!</strong></div>");
    return Utility::redirect("../../index.php");


}

//var_dump($_POST);