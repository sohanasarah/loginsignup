<?php
include_once("../../vendor/autoload.php");
session_start();
use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;
//var_dump($_POST);
$auth=new Auth();
$status= $auth->prepare($_POST)->is_exit();
if($status){
    Message::message("<div class='alert alert-danger'>Already Taken</div>");
    return Utility::redirect("../../index.php");

}
else{
    $obj= new User();
    $obj->prepare($_POST);
    $obj->store();

}
